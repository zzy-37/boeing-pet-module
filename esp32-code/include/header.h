#pragma once

#include <Arduino.h>

#define AUDIO_BUFFER_LEN 2048
#define AUDIO_BUFFER_COUNT 30

class Audio_Buffer
{
public:
    bool is_empty() { return m_is_empty; }
    bool is_full() { return !m_is_empty && m_read_ptr == m_write_ptr; }
    uint8_t *front() { return m_buffers[m_read_ptr]; }
    uint8_t *rear() { return m_buffers[m_write_ptr]; }
    void push()
    {
        if (is_full())
            return;
        if (++m_write_ptr == AUDIO_BUFFER_COUNT)
            m_write_ptr = 0;
        m_is_empty = false;
    }
    void pop()
    {
        if (is_empty())
            return;
        if (++m_read_ptr == AUDIO_BUFFER_COUNT)
            m_read_ptr = 0;
        if (m_read_ptr == m_write_ptr)
            m_is_empty = true;
    }
    uint8_t buffered_size()
    {
        if (m_is_empty)
            return 0;
        if (m_read_ptr < m_write_ptr)
            return m_write_ptr - m_read_ptr;
        return AUDIO_BUFFER_COUNT - m_read_ptr + m_write_ptr;
    }

private:
    uint8_t m_buffers[AUDIO_BUFFER_COUNT][AUDIO_BUFFER_LEN];
    bool m_is_empty = true;
    size_t m_read_ptr = 0;
    size_t m_write_ptr = 0;
};

extern Audio_Buffer buffers;

#include <DFRobot_MLX90614.h>
extern DFRobot_MLX90614_I2C temp_sensor;

extern const char* ip_address;

void display_init();
void display_clear();
void display_reset_cursor();
void display_show();
void display_print(const char* str);
void display_println(const char* str);
void display_set_cursor(int x, int y);
void display_start_info();
void display_update_info();

void start_webserver();