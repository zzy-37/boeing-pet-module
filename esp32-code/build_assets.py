import gzip

print('-----------------------------------')
print('Building assets.')

input_file_path = '../interface/interface.html'
print(f'Compress html page: {input_file_path}')

with open('include/index.html.gz.h', 'w') as out_file:
    with open(input_file_path, 'rb') as in_file:
        data = in_file.read()
        print(f'file size: {len(data)}')
        compressed = gzip.compress(data)
        print('compressed size:', len(compressed))
        out_file.write(
            f'const size_t index_html_gz_size = {len(compressed)};\n')
        out_file.write('const char index_html_gz[] PROGMEM = {\n')
        out_file.write(', '.join([hex(x) for x in compressed]))
        out_file.write('\n};\n')

print('-----------------------------------')
