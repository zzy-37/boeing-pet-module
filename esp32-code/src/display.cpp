#include "header.h"

#include "DEV_Config.h"
#include "GUI_Paint.h"
#include "utility/EPD_2in13_V3.h"

/* Fonts avaliable
extern sFONT Font24;
extern sFONT Font20;
extern sFONT Font16;
extern sFONT Font12;
extern sFONT Font8;
*/

#define FONT_L Font20

static unsigned str_width(const char* str)
{
    return FONT_L.Width * strlen(str);
}

static void print_str(int x, int y, const char* str)
{
    Paint_DrawString_EN(x, y, str, &FONT_L, WHITE, BLACK);
}

static void update_str(int x, int y, const char* str)
{
    Paint_ClearWindows(x, y, x + str_width(str), y + FONT_L.Height, WHITE);
    print_str(x, y, str);
}

static UBYTE image_buffer[(EPD_2in13_V3_WIDTH / 8 + 1) * EPD_2in13_V3_HEIGHT];

static int cursor_x, cursor_y;
static int base_x;

void display_print(const char* str)
{
    print_str(cursor_x, cursor_y, str);
    cursor_x += str_width(str);
}

void display_println(const char* str)
{
    print_str(cursor_x, cursor_y, str);
    cursor_x = base_x;
    cursor_y += FONT_L.Height;
}

void display_set_cursor(int x, int y)
{
    cursor_x = x;
    cursor_y = y;
    base_x = x;
}

void display_show()
{
    EPD_2in13_V3_Display(image_buffer);
}

void display_clear()
{
    Paint_Clear(WHITE);
    display_show();
}

void display_reset_cursor()
{
    display_set_cursor(0, 0);
}

void display_init()
{
    DEV_Module_Init();

    EPD_2in13_V3_Init();
    EPD_2in13_V3_Clear();

    Paint_NewImage(image_buffer, EPD_2in13_V3_WIDTH, EPD_2in13_V3_HEIGHT, 90, WHITE);
    Paint_Clear(WHITE);
    Paint_SelectImage(image_buffer);
}

void display_start_info()
{
    display_clear();
    display_reset_cursor();
    display_print("IP: ");
    display_println(ip_address);
    display_print("Temp: ");
}

void display_update_info()
{
    typedef unsigned long timestamp;
    timestamp loop_time = millis();
    static timestamp last_update_time = 0;

    if (loop_time > last_update_time + 3000) {
        float temp = temp_sensor.getObjectTempCelsius();

        char buff[7];
        snprintf(buff, sizeof(buff), "%.2fC", temp);
        update_str(cursor_x, cursor_y, buff);
        EPD_2in13_V3_Display_Partial(image_buffer);

        last_update_time = loop_time;
    }
}