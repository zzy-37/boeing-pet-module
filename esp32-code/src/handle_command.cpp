#include <Arduino.h>

void on_command(const char* cmd)
{
    if (cmd[0] == '\0')
        ;
    else
        Serial.printf("Unknown command: '%s'\n", cmd);
}

void handle_command()
{
    while (Serial.available()) {
        static char cmd_buf[32] = { 0 };
        static size_t cmd_size = 0;

        char c = Serial.read();
        switch (c) {
        case '\r':
            break;

        case '\n': {
            cmd_buf[cmd_size] = '\0';
            cmd_size = 0;
            Serial.write('\n');
            char* cmd = cmd_buf;
            while (*cmd == ' ' || *cmd == '\t')
                cmd++;
            on_command(cmd);
            Serial.write("> ");
            break;
        }

        case '\b':
            if (cmd_size > 0) {
                cmd_size--;
                Serial.write("\b \b");
            }
            break;

        default:
            if (cmd_size < sizeof(cmd_buf) - 1)
                cmd_buf[cmd_size++] = c;
            Serial.write(c);
            break;
        }
    }
}