#include "header.h"

#include <ESPmDNS.h>
#include <WiFiMulti.h>

#define SPEAKER_CONTROL_PIN 23
#define DAC_PIN 25
#define SAMPLE_RATE 8000

WiFiMulti wifiMulti;

Audio_Buffer buffers;
DFRobot_MLX90614_I2C temp_sensor;
const char* ip_address;

void ARDUINO_ISR_ATTR on_timer(void)
{
    if (!buffers.is_empty()) {
        static int counter = 0;
        digitalWrite(SPEAKER_CONTROL_PIN, LOW);
        dacWrite(DAC_PIN, buffers.front()[counter]);
        if (++counter >= AUDIO_BUFFER_LEN) {
            counter = 0;
            buffers.pop();
        }
    } else {
        digitalWrite(SPEAKER_CONTROL_PIN, HIGH);
        pinMode(DAC_PIN, INPUT);
    }
}

void setup()
{
    Serial.begin(115200);

    pinMode(SPEAKER_CONTROL_PIN, OUTPUT);
    digitalWrite(SPEAKER_CONTROL_PIN, HIGH);

    Serial.println("Initializing display...");
    display_init();

    display_println("Init temp sensor");
    display_show();

    Serial.println("Initialize temp sensor");
    while (NO_ERR != temp_sensor.begin()) {
        Serial.println("Communication with device failed, please check connection");
        delay(3000);
    }
    Serial.println("Temp Begin ok!");

    // setup timer
    // clock frequency 80Mhz
    // divided by devider we get timer frequency
    hw_timer_t* timer = timerBegin(0, 10 /*divider*/, true /*count_up*/);
    timerAttachInterrupt(timer, on_timer, true);
    timerAlarmWrite(timer, 8000000 / SAMPLE_RATE, true); // counter = 80000000 / divider / SAMPLE_RATE
    timerAlarmEnable(timer);

    wifiMulti.addAP("Xiaomi_16CC", "zzzzssss");
    wifiMulti.addAP("Fablab O", "fablabshanghai");
    wifiMulti.addAP("ChinaNet-jyfE", "haapdegf");

    display_clear();
    display_reset_cursor();
    display_println("Connecting Wifi...");
    display_show();

    Serial.println("Connecting Wifi...");
    while (wifiMulti.run() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    ip_address = WiFi.localIP().toString().c_str();
    Serial.println(ip_address);

    if (MDNS.begin("esp32"))
        Serial.println("MDNS responder started");

    start_webserver();

    display_start_info();
}

void loop()
{
    if (buffers.is_empty())
        display_update_info();
}