#include "header.h"
#include "index.html.gz.h"

#include <esp_http_server.h>

static esp_err_t ws_handler(httpd_req_t *req)
{
    if (req->method == HTTP_GET)
    {
        Serial.println("Info: Handshake done, the new connection was opened");
        return ESP_OK;
    }

    httpd_ws_frame_t ws_pkt{};

    while (buffers.is_full())
        Serial.println("Audio buffer is full, blocking socket data transfer.");

    ws_pkt.payload = buffers.rear();

    esp_err_t ret = httpd_ws_recv_frame(req, &ws_pkt, AUDIO_BUFFER_LEN);
    if (ret != ESP_OK)
    {
        Serial.printf("Error: httpd_ws_recv_frame failed to get frame len with %d\n", ret);
        return ret;
    }

    if (ws_pkt.len == AUDIO_BUFFER_LEN && ws_pkt.type == HTTPD_WS_TYPE_BINARY)
        buffers.push();
    else
        Serial.printf("Got non audio packet, type: %d, len: %d\n", ws_pkt.type, ws_pkt.len);

    return ret;
}

void start_webserver(void)
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    httpd_handle_t server = NULL;

    httpd_uri_t uri_get = {
        .uri = "/",
        .method = HTTP_GET,
        .handler = [](httpd_req_t *req) -> esp_err_t
        {
            httpd_resp_set_type(req, "text/html");
            httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
            return httpd_resp_send(req, index_html_gz, sizeof(index_html_gz));
        },
        .user_ctx = NULL
#ifdef CONFIG_HTTPD_WS_SUPPORT
        ,
        .is_websocket = true,
        .handle_ws_control_frames = false,
        .supported_subprotocol = NULL
#endif
    };

    httpd_uri_t uri_ip = {
        .uri = "/ip",
        .method = HTTP_GET,
        .handler = [](httpd_req_t *req) -> esp_err_t
        {
            char url[32];
            snprintf(url, 32, "http://%s/", ip_address);

            httpd_resp_set_status(req, "301 Redirect");
            httpd_resp_set_hdr(req, "Location", url);
            return httpd_resp_send(req, NULL, 0);
        },
        .user_ctx = NULL
#ifdef CONFIG_HTTPD_WS_SUPPORT
        ,
        .is_websocket = true,
        .handle_ws_control_frames = false,
        .supported_subprotocol = NULL
#endif
    };

    httpd_uri_t uri_temp = {
        .uri = "/temp",
        .method = HTTP_GET,
        .handler = [](httpd_req_t *req) -> esp_err_t
        {
            char temp[6];
            size_t len = snprintf(temp, sizeof(temp), "%.2f", temp_sensor.getObjectTempCelsius());
            return httpd_resp_send(req, temp, len);
        },
        .user_ctx = NULL
#ifdef CONFIG_HTTPD_WS_SUPPORT
        ,
        .is_websocket = true,
        .handle_ws_control_frames = false,
        .supported_subprotocol = NULL
#endif
    };

    httpd_uri_t uri_ws = {
        .uri = "/ws",
        .method = HTTP_GET,
        .handler = ws_handler,
        .user_ctx = NULL
#ifdef CONFIG_HTTPD_WS_SUPPORT
        ,
        .is_websocket = true,
        .handle_ws_control_frames = false,
        .supported_subprotocol = NULL
#endif
    };

    if (httpd_start(&server, &config) == ESP_OK)
    {
        httpd_register_uri_handler(server, &uri_get);
        httpd_register_uri_handler(server, &uri_ip);
        httpd_register_uri_handler(server, &uri_ws);
        httpd_register_uri_handler(server, &uri_temp);
    }
    else
        Serial.println("Failed to start server.");
}