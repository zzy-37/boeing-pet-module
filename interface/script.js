function update_info() {
    window['el-window-width'].textContent = `width: ${innerWidth}`
    window['el-window-height'].textContent = `height: ${innerHeight}`
}

function update_canvas_size() {
    cnv1.height = cnv1.clientHeight
    cnv2.width = cnv2.clientWidth
}

onresize = () => {
    update_info()
    update_canvas_size()
}

update_info()
update_canvas_size()

const ctx1 = cnv1.getContext('2d')
const min_temp = 37
const max_temp = 41

const get_temp = (function () {
    let temp = 39
    return function () {
        temp += (Math.random() - 0.5) * 0.2
        return temp
    }
})()

const css_var = name => getComputedStyle(document.documentElement).getPropertyValue(name)

const temp2y = (cnv, temp) => (1 - (temp - min_temp) / (max_temp - min_temp)) * cnv.height

function start_logging() {
    const temp_values = []
    const start_time = performance.now()

    let temp_min, temp_max

    const intervalID = setInterval(() => {
        const temp = get_temp()

        if (!temp_min) temp_min = temp_max = temp
        if (temp > temp_max) temp_max = temp
        if (temp < temp_min) temp_max = temp

        window['el-temp-val'].textContent = temp.toFixed(1)
        window['el-temp-min'].textContent = temp_min.toFixed(1)
        window['el-temp-max'].textContent = temp_max.toFixed(1)

        temp_values.push({
            time: (performance.now() - start_time) / 1000,
            value: temp
        })
    }, 1000)

    let requestID
    function draw() {
        ctx1.clearRect(0, 0, cnv1.width, cnv1.height)

        {
            ctx1.strokeStyle = css_var('--color-red')
            ctx1.beginPath()
            const y = temp2y(cnv1, 40)
            ctx1.moveTo(0, y)
            ctx1.lineTo(cnv1.width, y)
            ctx1.stroke()
        }

        {
            ctx1.strokeStyle = css_var('--color-blue')
            ctx1.beginPath()
            const y = temp2y(cnv1, 38.5)
            ctx1.moveTo(0, y)
            ctx1.lineTo(cnv1.width, y)
            ctx1.stroke()
        }

        ctx1.strokeStyle = css_var('--color-gray2')
        ctx1.beginPath()
        temp_values.forEach(({ value, time }, i) => {
            const x = time / 60 * cnv1.width
            const y = temp2y(cnv1, value)
            if (i == 0) ctx1.moveTo(x, y)
            else ctx1.lineTo(x, y)
        });
        ctx1.stroke()

        requestID = requestAnimationFrame(draw)
    }

    draw()
}


start_logging()