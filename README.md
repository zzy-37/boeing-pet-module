Links:
- esp32 e-paper module: https://www.waveshare.net/wiki/2.13inch_e-Paper_Cloud_Module
- record audio from web page: https://web.dev/media-recording-audio/
- esp32 wroom 32 datasheet: https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf
- esp32 http server: https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/protocols/esp_http_server.html
- esp32 arduino reference: https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/libraries.html
- arduino audio player: https://docs.arduino.cc/tutorials/generic/simple-audio-player
- esp32 audio speaker: https://circuitdigest.com/microcontroller-projects/esp32-based-audio-player